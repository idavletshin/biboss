$(document).ready(function () {
  $('#form-block').submit(function (e) {
    e.preventDefault();
    let phone = $('#phone').val();

    $(".error").remove();

    if (phone.length < 6) {
      $('.error-phone input').addClass('input--error');
      $('.error-phone').after('<span class="error">Укажите правильный телефон</span>');
      return false;
    } else {
      $('.error-phone input').removeClass('input--error');
    }

    $('.rfield').val('');
    $('.button').prop('disabled', true);
    $('.button').removeClass('button--active');
    inputsVal = { name: false, phone: false, user_franchise: false }

  });

  const franchises = ['Проворный ткачик', 'ПРОФИ-СПОРТ', 'Проспект', 'Просто чудо', 'Прокат Пони+'];
  let inputsVal = { name: false, phone: false, user_franchise: false }

  $("#name").on('change keyup paste', function () {
    if ($(this).val().length > 0) {
      inputsVal.name = true
    } else {
      inputsVal.name = false
    }
  });

  $("#phone").on('change keyup paste', function () {
    if ($(this).val().length > 0) {
      inputsVal.phone = true
    } else {
      inputsVal.phone = false
    }
  });

  $("#user_franchise").on('change keyup paste', function () {
    if ($(this).val().length > 0) {
      inputsVal.user_franchise = true
      let display = $("#ui-id-1").css("display");
      if (display != "none") {
        $(this).addClass("radius-none");
      } else {
        $(this).removeClass("radius-none")
      }
    } else {
        $(this).removeClass("radius-none")
      inputsVal.user_franchise = false
    }
  });

  $(".rfield").on('change keyup paste', function () {
    if (inputsVal.phone === true && inputsVal.name === true && inputsVal.user_franchise === true) {
      $('.button').prop('disabled', false);
      $('.button').addClass('button--active');
    } else {
      $('.button').prop('disabled', true);
      $('.button').removeClass('button--active');
    }
  });

  $(function () {
    $('#phone').on('keypress', function (e) {
      if (!/[0-9 -()+]/.test(String.fromCharCode(e.which))) {
        return false;
      }
    }).on('paste', function () {
      return false;
    })
  })

  $("#user_franchise").autocomplete({
    source: franchises,
    delay: 0,
  });
  $(window).click(function(e) {
    $("#user_franchise").removeClass("radius-none")
  });

});